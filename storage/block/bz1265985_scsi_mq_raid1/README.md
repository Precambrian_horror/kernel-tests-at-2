# storage/block/bz1265985_scsi_mq_raid1
Storage: bz1265985_scsi_mq_raid1

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
